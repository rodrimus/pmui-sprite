require 'rubygems'

desc "check all requirements"
task :required do
    isOK = true
    puts "checking all the dependencies..."
    begin
        require 'json'
    rescue LoadError
        puts "=> JSON gem not found.\nInstall it by running 'gem install json'"
        isOk = false
    end
	begin
		require "compass"
	rescue LoadError
		isOK = false
		puts "=> Compass GEM required, install by running 'gem install compass'"
	endsOk = false
    end
	if !isOK
		exit
	end
	puts "DONE"
end

desc "compile stylesheets"
task :styles do 
    puts "generating stylesheets..."
    command = "compass compile"
    styles_file = File.read 'config/styles.json'
    styles = JSON.parse(styles_file)
    styles.each do |style|
        command += ' --sass-dir "' + style['sass_dir'] + '"'
        command += ' --css-dir "' + style['css_dir'] + '"'
        command += ' --images-dir "' + style['img_dir'] + '"'
        command += ' --javascripts-dir "' + style['js_dir'] + '"'
        command += ' --fonts-dir "' + style['fonts_dir'] + '"'
        command += ' --output-style "' + style['output_style'] + '"'
        command += style['relative_assets'] ? ' --relative-assets' : ''
        command += style['comments'] ? '' : ' --no-line-comments'
        command += style['debug'] ? ' --debug-info' : ' --no-debug-info'
        command += ' --force'
    end

    system command
    puts "DONE"
end

desc "build the project results"
task :build do
	puts "initializing project building..."
	Rake::Task['required'].execute
	Rake::Task['styles'].execute
	puts "project built successfully!"
end

desc "the default task"
task :default do
	Rake::Task['build'].execute
end